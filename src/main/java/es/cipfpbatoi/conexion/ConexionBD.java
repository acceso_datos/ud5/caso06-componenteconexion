package es.cipfpbatoi.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

// Patrón singleton
public class ConexionBD {
	private static String Ip = "192.168.56.101";
	private static String Bd = "empresa";      	// batoi?currentSchema=empresa_ad"
	private static String Jdbc = "jdbc:mariadb";	// jdbc:postgresql
	private static String user = "batoi";
	private static String password = "1234";
    
    private static Connection con = null;    

    public static Connection getConexion() throws SQLException {
        
    	if (con == null) {
    		String JdbcUrl = Jdbc + "://" + Ip + "/" + Bd; 
            Properties pc = new Properties();
            pc.put("user", user);
            pc.put("password", password);
            con = DriverManager.getConnection(JdbcUrl, pc);
        }
        return con;
    }

    public static void cerrar() throws SQLException {
        if (con != null) {
            con.close();
            con = null;
        }
    }

	public static String getIp() {
		return Ip;
	}

	public static void setIp(String ip) {
		Ip = ip;
	}

	public static String getBd() {
		return Bd;
	}

	public static void setBd(String bd) {
		Bd = bd;
	}

	public static String getJdbc() {
		return Jdbc;
	}

	public static void setJdbc(String jdbc) {
		Jdbc = jdbc;
	}

	public static String getUser() {
		return user;
	}

	public static void setUser(String user) {
		ConexionBD.user = user;
	}

	public static String getPassword() {
		return password;
	}

	public static void setPassword(String password) {
		ConexionBD.password = password;
	}    
	
	
    

}
